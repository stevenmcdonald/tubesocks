# tubesocks

Socks5 tunnel to handle authentication

Do you have a SOCKS5 client that doesn't support authentication (like Chrome) that needs to connect to a SOCKS5 server that requries authentication? Then tubesocks is for you.

Originally based on https://github.com/armon/go-socks5, tubesocks preserves just enough functionality to listen an a SOCKS5 server that doesn't require auth, with added functionality to tunnel the connection to an upsteam SOCKS5 server with authentication.

# example

```go
package main

import (
	"gitlab.com/stevenmcdonald/tubesocks"
)


func main() {

	user := "username"
	password := "password"
	proxyURL := "127.0.0.1:8181"
	listenPort := 9191

	tubesocks.Start(user, password, proxyURL, listenPort)
}
```
